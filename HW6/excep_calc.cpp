#include <iostream>

#define ERR_8200 "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."
#define ILLEGAL 8200

int add(int a, int b) {
	if (a == ILLEGAL || b == ILLEGAL || a + b == ILLEGAL)
		throw std::string(ERR_8200);
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void) {
	try { 
		std::cout << "multiply result: " << multiply(3, 4) << std::endl;
	}
	catch (std::string& err) {
		std::cout << err << std::endl;
	}
	try {
		std::cout << "add result: " << add(8199, 1) << std::endl;
	}
	catch (std::string err) {
		std::cout << err << std::endl;
	}
	try {
		std::cout << "add result: " << add(3000, 222) << std::endl;
	}
	catch (std::string err) {
		std::cout << err << std::endl;
	}

	try {
		std::cout << "pow result: " << pow(8200, 3) << std::endl;
	}
	catch (std::string err) {
		std::cout << err << std::endl;
	}
	try {
		std::cout << "pow result: " << pow(3000, 2) << std::endl;
	}
	catch (std::string err) {
		std::cout << err << std::endl;
	}

}