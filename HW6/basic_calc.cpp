#include <iostream>
#define ILLEGAL 8200

bool add(int a, int b, int& result) {
	if (a != ILLEGAL && b != ILLEGAL && a + b != ILLEGAL) {
		result = a + b;
		return true;
	}
	return false;
}

bool  multiply(int a, int b, int& result) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		if (!add(sum, a, sum)) {
			return false;
		}
	};
	result = sum;
	return true;
}

bool pow(int a, int b, int& result) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		if (!multiply(exponent, a, exponent)) {
			return false;
		}
	};
	result = exponent;
	return true;
}

void printErr()
{
	std::cout << "This user is not authorized to" <<
		"access 8200, please enter different numbers, or try to get .clearance in 1 year" << std::endl;
}

int main(void) {
	int x = 0;

	if (!multiply(3, 4, x)) printErr();
	else std::cout << "multiply result: " << x << std::endl;
	if (!multiply(4100, 3, x)) printErr();
	else std::cout << "multiply result: " << x << std::endl;

	if (!add(8199, 1, x)) printErr();
	else std::cout << "add result: " << x << std::endl;
	if (!add(3000, 322, x)) printErr();
	else std::cout << "add result: " << x << std::endl;

	if (!add(8200, 3, x)) printErr();
	else std::cout << "power result: " << x << std::endl;
	if (!add(3000, 2, x)) printErr();
	else std::cout << "power result: " << x << std::endl;
}