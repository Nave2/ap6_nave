#pragma once
#include <cmath>

class MathUtils
{
public:
	static double CalPentagonArea(double sideLen);
	static double CalHexagonArea(double sideLen);
private:
	static double calPolygonArea(int sides, double sideLen);
};