#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Pentagon : public Shape
{
public:
	// shape methods declartion prevent from setting const methods here...
	Pentagon(std::string name, std::string color, double len);
	~Pentagon();
	void draw();
	double CalArea();
	void setSideLen(double len);
private:
	double _sideLen;
};