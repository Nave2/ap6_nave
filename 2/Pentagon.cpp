#include "Pentagon.h"

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << this->getColor() << std::endl << "Name is " << this->getName() << std::endl << "side len is " << this->_sideLen << std::endl << "Area: " << this->CalArea() << std::endl;;
}

Pentagon::Pentagon(std::string name, std::string color, double len) : Shape(name, color)
{
	this->setSideLen(len);
}

Pentagon::~Pentagon()
{

}

double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(this->_sideLen);
}

void Pentagon::setSideLen(double len)
{
	if (len < 0)
		throw shapeException();
	this->_sideLen = len;
}
