#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"

class Hexagon : public Shape
{
public:
	// shape methods declartion prevent from setting const methods here...
	Hexagon(std::string name, std::string color, double len);
	~Hexagon();
	void draw();
	double CalArea();
	void setSideLen(double len);
private:
	double _sideLen;
};