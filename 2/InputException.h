#pragma once
#include <exception>

class InputException : public std::exception
{
public:
	std::string content() const
	{
		return "This is a input exception! Got an unexpected value";
	}
};