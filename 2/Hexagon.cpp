#include "Hexagon.h"

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << this->getColor() << std::endl << "Name is " << this->getName() << std::endl << "side len is " << this->_sideLen << std::endl << "Area: " << this->CalArea() << std::endl;;
}

Hexagon::Hexagon(std::string name, std::string color, double len) : Shape(name, color)
{
	this->setSideLen(len);
}

Hexagon::~Hexagon()
{

}

double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(this->_sideLen);
}

void Hexagon::setSideLen(double len)
{
	if (len < 0)
		throw shapeException();
	this->_sideLen = len;
}
