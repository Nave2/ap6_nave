#include "MathUtils.h"

double MathUtils::CalPentagonArea(double sideLen)
{
	return MathUtils::calPolygonArea(5, sideLen);
}

double MathUtils::CalHexagonArea(double sideLen)
{
	return MathUtils::calPolygonArea(6, sideLen);
}

double MathUtils::calPolygonArea(int sides, double sideLen)
{
	return 0.25 * sides * sideLen * sideLen * (1.0 / tan((atan(1) * 4) / sides));
}
